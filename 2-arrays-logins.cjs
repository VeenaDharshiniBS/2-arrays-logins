/*
const personDetails = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}];
*/

function agender(data)
{
    return data.filter((data)=>data.gender=='Agender');
}

//console.log(agender(personDetails));

function ipv4AddressComponents(data)
{
    const res = data.map((person)=>{
        let ipAdress = person.ip_address.split('.');
        return ipAdress.map((ele)=>parseInt(ele));
    });
    return res;
}
//console.log(ipv4AddressComponents(personDetails));

function secondComponentsSum(data)
{
    const ipv4Address = ipv4AddressComponents(data);
    return ipv4Address.reduce((acc,address)=>acc+address[1],0);
}

//console.log(secondComponentsSum(personDetails));

function fourthComponentsSum(data)
{
    const ipv4Address = ipv4AddressComponents(data);
    return ipv4Address.reduce((acc,address)=>acc+address[3],0);
}

//console.log(fourthComponentsSum(personDetails));

function fullName(data)
{
    return data.map((data)=>{
        data.full_name=data.first_name+" "+data.last_name;
        return data;
    });
}

//console.log(fullName(personDetails));

function orgEmails(data)
{
    return data.filter(person=>{
        if(person.email.includes(".org"))
            return person;
    });
}

//console.log(orgEmails(personDetails));

function sumOfOrgAuCom(data)
{
    let sum = 0;
    data.map(person=>{
        if(person.email.includes("org"))
            sum += 1;
        else if(person.email.includes(".au"))
            sum += 1;
        else if(person.email.includes(".com"))
            sum += 1;
    });
    return sum;
}

//console.log(sumOfOrgAuCom(personDetails));

function sortByFirstName(data)
{
    return data.sort(function(a, b) {
        if(a.first_name>b.first_name)
            return -1;
        else if(a.first_name<b.first_name)
            return 1;
        else
            return 0;
    });
}

//console.log(sortByFirstName(personDetails));